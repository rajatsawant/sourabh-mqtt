const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const constants = require('../services/constants');

const rpmSchema = mongoose.Schema({
  trip_id: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'trips'
  },
  rpm: {
    type: Number,
    required: true
  }
});

const rpmModel = module.exports = mongoose.model('rpms', rpmSchema);

module.exports.logRPM = function (trip_id, rpm) {
  var obj = {
    trip_id: trip_id,
    rpm: rpm
  }
  new rpmModel(obj).save(function(err, resp){
    // console.log(err, resp);
  });
}