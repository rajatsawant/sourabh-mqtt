const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;
const userDB = require('../models/user_data');

const relative_dataSchema = mongoose.Schema({
	subscriberid: {
		type: Schema.Types.ObjectId,
		required: true,
		ref: 'subscribers'
	},
	name: {
		type: String,
		required: true
	},
	contactno: {
		type: String,
		required: true
	},
	emailid: {
		type: String,
		required: false
	},
	relation: {
		type: String,
		required: true
	},
});

const relativeModel = module.exports = mongoose.model('relatives', relative_dataSchema);

//Add relative
module.exports.create = function (newRelative, callback) {
	userDB.findById(newRelative.subscriberid, function (err, resp) {   //Check if valid subscriber id
		if (resp)
			new relativeModel(newRelative).save(callback);
		else {
			callback(err ? err : { reason: "Subscriber id invalid or not found" }, null);
		}
	});
}

//Delete relative
module.exports.delete = function (relative, callback) {
	relativeModel.findOneAndRemove(relative, callback);
}

module.exports.getRelativesBySubscriberEmail = function (email, callback) {
	userDB.findOne({emailid:email}, function (err, resp) {   //Check if valid subscriber id
		if (resp){
			var query = { subscriberid: resp._id };
			relativeModel.find(query, callback);
		} else {
			callback(err ? err : { reason: "Subscriber id invalid or not found" }, null);
		}
	});
	
}

// module.exports.getUserby = function (useremail, callback) {
//     console.log(useremail);
//     var obj;
//     var query = { subscriber_email: useremail };
//     var dat = [];
//     // databass.find(query,callback);
//     databass.find(query, function (err, docs) {
//         //console.log(docs);
//         dat.push(docs);
//     });
// } 