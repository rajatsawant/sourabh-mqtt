const mongoose = require('mongoose');
const userDB = require('../models/user_data');
const Schema = mongoose.Schema;
const constants = require('../services/constants');

const carSchema = mongoose.Schema({
	subscriberid: {
		type: Schema.Types.ObjectId,
		required: true,
		ref:'subscribers'
	},
	car_id: {
		type: String,
		required: true,
		unique: true
	},
	manufacturer: {
		type: String,
		required: true
	},
	model: {
		type: String,
		required: true
	},
	vin: {
		type: String,
		required: true
	},
	reg_no: {
		type: String,
		required: true
	},
	insaurance_details: {
		type: String,
		required: true
	},
	deviceserno: {
		type: String,
		required: true
	},
	current_trip: {
		type: Schema.Types.ObjectId,
		ref: 'trips',
		required: false
	},
	car_status: {
		type: String
	},
	speed_limit: {
		type: Number,
		required: false
	},
	speed_alert: {
		type: String,
		required: false
	},
	ignition_alert: {
		type: String,
		required: false
	},
	geofence_alert: {
		type: String,
		required: false
	}
});

const carModel = module.exports = mongoose.model('cars', carSchema);

module.exports.create = function(car, callback) {
	var userModel = require('../models/user_data');
	userModel.findById(car.subscriberid, function (err, resp) {   //Check if valid subscriber id
		if (resp)
			new carModel(car).save(callback);
		else {
			callback(err ? err : { reason: "Subscriber id invalid or not found" }, null);
		}
	});
}

module.exports.delete = function (car, callback) {
	carModel.findOneAndRemove(car, callback);
}

module.exports.getCarsbySubscriberId = function(subscriberid, callback) {
	userDB.findById(subscriberid, function (err, resp) {   //Check if valid subscriber id
		if (resp){
			var query = {subscriberid:subscriberid};
			carModel.find(query, callback);
		} else {
			callback(err ? err : { reason: "Subscriber id invalid or not found" }, null);
		}
	});
}

module.exports.setSpeedLimit = function (id, limit, callback){
	carModel.findOneAndUpdate({car_id:id}, {speed_limit: limit}, constants.options.update, callback);
}