const mongoose = require('mongoose');
const carDB = require('../models/cars_data');
const Schema = mongoose.Schema;
const constants = require('../services/constants');

const tripSchema = mongoose.Schema({
  deviceid: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'cars'
  },
  subscriberid: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'subscribers'
  },
  starttime: {
    type: Date,
    required: true
  },
  endtime: {
    type: Date,
    required: false
  },
  mileage: {
    type: Number,
    required: false
  },
  distance: {
    type: Number,
    required: false
  },
  path: {
    type: Array,
    required: false
  },
  startlat: {
    type: Number,
    required: true
  },
  startfuel:{
    type: String,
    required:false
  },
  endfuel: {
    type: String,
    required: false
  },
  startlon: {
    type: Number,
    required: true
  },
  endlat: {
    type: Number,
    required: false
  },
  endlong: {
    type: Number,
    required: false
  },
  status: {
    type: String,
    required: true
  }
});

const tripModel = module.exports = mongoose.model('trips', tripSchema);

module.exports.startTrip = function(trip, callback) {
	carDB.findById(trip.deviceid, function (err, resp) {   //Check if valid subscriber id
    if (resp){
      trip.subscriberid = resp.subscriberid;
      trip.starttime = new Date();
      trip.status = constants.TRIP_STARTED;
      new tripModel(trip).save(callback);
    } else {
			callback(err ? err : { reason: "Device id invalid or not found" }, null);
		}
	});
}

module.exports.stopTrip = function(params, trip, callback) {
  var query = {deviceid:params.deviceid, status:constants.TRIP_STARTED};
	tripModel.findOne(query, function(err, resp) {
    if (resp) {
      trip.endtime = new Date();
      trip.status = constants.TRIP_STOPPED; 
      tripModel.findByIdAndUpdate(resp._id, trip, {new: true}, callback);
    } else {
      callback(err ? err : { reason: resp ? "Trip is stopped" : "Trip not found" }, null);
    }
  });
}

module.exports.getTripsbyDeviceId = function(device_id, callback) {
	carDB.findOne({deviceid: device_id}, function (err, resp) {   //Check if valid subscriber id
		if (resp){
			var query = {deviceid:device_id};
			tripModel.find(query, callback);
		} else {
			callback(err ? err : { reason: "Device id invalid or does not exist." }, null);
		}
	});
}

module.exports.getTripsBySubscriberId = function(subscriberid, callback) {
  tripModel.find({subscriberid: subscriberid}, callback);
}

module.exports.updatePath = function (id, location) {
  tripModel.findByIdAndUpdate(id, { $push: { path: location }}, function(err, resp) {
    // console.log(err, resp);
  });
}