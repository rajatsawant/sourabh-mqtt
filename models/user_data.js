const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const carDB = require('../models/cars_data');
const constants = require('../services/constants');
const toolbox = require('../services/toolbox');

const user_dataSchema = mongoose.Schema({
	subscriber_name: {
		type: String,
		required: true
	},
	age: {
		type: String,
		required: true
	},
	gender: {
		type: String,
		required: true
	},
	blood_grp: {
		type: String,
		required: true
	},
	emailid: {
		type: String,
		required: true,
		unique: true
	},
	password: {
		type: String,
		required: true
	},
	contactno: {
		type: String,
		required: true
	},
	address: {
		type: String,
		required: true
	},
	state: {
		type: String,
		required: true
	},
	city: {
		type: String,
		required: true
	},
	country: {
		type: String,
		required: true
	},
	dl_no: {
		type: String,
		required: true
	},
	createdate: {
		type: String,
		required: true
	},
	updatedate: {
		type: String,
		required: true
	},
	fcm_token: {
		type: String,
		required: false
	}
});

const subscriberModel = module.exports = mongoose.model('subscriber', user_dataSchema);

//new user
module.exports.create = function (newUser, callback) {
	if (!newUser.car_id) {
		callback({ error: "car_id not present" });
		return;
	}
	bcrypt.genSalt(10, function (err, salt) {
		bcrypt.hash(newUser.password, salt, function (err, hash) {
			// Store hash in your password DB.
			newUser.password = hash;

			//Add current date to createdate and updatedate 
			var nowDate = new Date();
			newUser.createdate = toolbox.yyyymmdd(nowDate);
			newUser.updatedate = toolbox.yyyymmdd(nowDate);
			//store to db
			new subscriberModel(newUser).save(function (err, user) {
				if (user) {
					var defaultCar = {
						subscriberid: user._id,
						car_id: newUser.car_id,
						manufacturer: constants.default,
						model: constants.default,
						vin: constants.default,
						reg_no: constants.default,
						insaurance_details: constants.default,
						deviceserno: constants.default
					}
					new carDB(defaultCar).save(function(err, car){
						if(car){
							callback(null, user);
						}
						else {
							subscriberModel.findByIdAndRemove(user._id, function(){});
							callback(err);
						}	
					});
				} else {
					callback(err);
				}
			});
		});
	});
}

//update user
module.exports.update = function (id, updateObject, callback) {
	updateObject.updatedate = toolbox.yyyymmdd(new Date());
	subscriberModel.findByIdAndUpdate(id, updateObject, { new: true }, callback);
}

// module.exports.getUserbyName = function (username, callback) {
//     console.log("getting in module");
//     var query = { username: subscriber_name };
//     subscriberModel.findOne(query, callback);

// }
// module.exports.getUserby = function (username, callback) {
//     console.log(username);
//     var obj;
//     var query = { subscriber_name: username };
//     databass.findOne(query, callback);
// }

module.exports.comparePassword = function (password, hash, callback) {
	bcrypt.compare(password, hash, function (err, isMatch) {
		if (err) throw err;
		callback(null, isMatch);
	});
}

//Pass the _id
module.exports.getUserbyID = function (id, callback) {
	subscriberModel.findById(id, callback);
} 