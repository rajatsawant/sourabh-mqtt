const mongoose = require('mongoose');
const userDB = require('../models/user_data');
const Schema = mongoose.Schema;

const geofenceSchema = mongoose.Schema({
	subscriberid: {
    type: Schema.Types.ObjectId,
    ref: 'subscribers',
		required: true
  },
  lat: {
    type: Number,
    required: true
  },
  long: {
    type: Number,
    required: true
  },
  radius: {
    type: Number,
    required: true
  }
});

const geofenceModel = module.exports = mongoose.model('geofences', geofenceSchema);

module.exports.create = function(geofence, callback) {
  new geofenceModel(geofence).save(callback);
}

module.exports.delete = function (geofence, callback) {
	geofenceModel.findOneAndRemove(geofence, callback);
}

module.exports.getGeofences = function(subscriberid, callback) {
	geofenceModel.find({subscriberid:subscriberid}, callback);
}