module.exports = {
	URL: '/leoback_linux_3.0',
	ERROR: 'error',
	SUCCESS: 'success',
	TRIP_STARTED: 'started',
	TRIP_STOPPED: 'stopped',
	CAR_RUNNING: 'running',
	CAR_STOPPED: 'off',
	FCM_FLAGS: {
		//speed limit
		SPEED_LIMIT_CROSSED_SENT: 1,
		SPEED_LIMIT_NORMAL_SENT: 2,
		SPEED_LIMIT_RESET: 3,
		//ignition
		IGNITION_ON_SENT: 1,
		IGNITION_OFF_SENT: 2,
		IGNITION_RESET: 3,
		//geofence
		GEOFENCE_IN_SENT: 1,
		GEOFENCE_OUT_SENT: 2
	},
	MQTT: {
		RPM: "eng",
		SPEED: "speed",
		GRAVITATION_FORCE: "gf",
		LOCATION_COORDS: "cord"
	},
	routes: {
		user: {
			LOGIN: "/login",
			REGISTER: "/create"
		},
		relative: {
			ADD: '/create',
			DELETE: '/delete',
			GET_RELATIVES_BY_SUBSCRIBER_ID: '/findrelativebysub_id/:emailid'
		},
		cars: {
			ADD: '/create',
			DELETE: '/delete',
			GET_CARS_BY_SUBSCRIBER_ID: '/getCarsBySubscriberId',
			SET_SPEED_LIMIT: '/setSpeedLimit'
		},
		trip: {
			START_TRIP: '/create',
			STOP_TRIP: '/end/:deviceid',
			DELETE: '/delete',
			GET_TRIPS_BY_SUBSCRIBER_ID: '/getalltrips/:id'
		},
		geofence: {
			ADD: '/add',
			DELETE: '/delete',
			GET_GEOFENCES_BY_SUBSCRIBER_ID: '/getGeofencesBySubscriberId'
		}
	},
	options: {
		update:{
			new: true
		}
	},
	default: 'Default'
}