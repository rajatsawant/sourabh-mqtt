var admin = require('../config/firebase');
var userDB = require('../models/user_data');
module.exports = {
  usingToken: function(token, title, body, data, callback){
    var message = {
      token: token,
      notification: {
        title: title,
        body: body
      }
    }
    if(data)
      message.data = data;
    admin.messaging().send(message)
      .then(function(){
        callback(true);
      })  //success callback
      .catch(function(err){
        console.log("Error send notification to user");
        console.log(err);
        callback(false)
      });
  },
  usingUserId: function(id, title, body, data, callback) {
    userDB.findById(id, function(err, resp) {
      if (resp && resp.fcm_token) {
        var message = {
          token: resp.fcm_token,
          notification: {
            title: title,
            body: body
          }
        }
        if(data) message.data = data;
        admin.messaging().send(message)
          .then(function(){
            callback(true);
          })  //success callback
          .catch(function(err){
            console.log("Error send notification to user");
            console.log(err);
            callback(false);
          });
      }
    });
  }
}