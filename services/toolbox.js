const constants = require('../services/constants');

module.exports = {
	/**
		* Generalised method to send response of api in a structured way
		* @function
		* @param {object} res Express response object
		* @param {string} type success/error
		* @param {string} msg Any message to return in api response
		* @param {object} data Any data to return in api response
	*/
	sendResponse: function (res, type, data, msg) {
		if (data instanceof Object) {
			delete data.__v;
		}

		switch (type) {
			case constants.SUCCESS:
				res.send(data);
				break;
			case constants.ERROR:
				var obj = {
					errorMsg: msg,
					error: data
				};
				res.json(deleteUndefinedandNullKeys(obj));
				break;
			default:
			  var obj = {
					msg: msg ? msg : null,
					data: data ? data: null
				};
				res.json(deleteUndefinedandNullKeys(obj));
		}
	},

	yyyymmdd: function(date) {
		var mm = date.getMonth() + 1; // getMonth() is zero-based
		var dd = date.getDate();
		var parsed = [date.getFullYear(),
			(mm>9 ? '' : '0') + mm,
			(dd>9 ? '' : '0') + dd
		 ].join('-');
		return parsed;
	}
}

function deleteUndefinedandNullKeys(obj) {
	Object.keys(obj).forEach(key => {
		if (obj[key] === undefined || obj[key] === null) {
			delete obj[key];
		}
	});
	return obj;
} 