# Sourabh Mqtt

## Setup Instructions

### Clone repo
```sh
$ git clone https://rajatsawant@bitbucket.org/rajatsawant/sourabh-mqtt.git
```
Enter your login credentials.

### Go to folder.
```sh
$ cd sourab-mqtt
```

### Install node modules
```sh
$ npm install
```

### Run server
```sh
$ node mqtt_server.js
``` 