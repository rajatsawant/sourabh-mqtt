var mqtt = require('mqtt');
var client = mqtt.connect('tcp://localhost:1883');
const carDB = require('../models/cars_data');
const tripDB = require('../models/trip_data');
const constants = require('../services/constants');
const notify = require('../services/notify');
const rpmDB = require('../models/rpm_data');
const geofenceDB = require('../models/geofence_data');
const geolib = require('geolib');

client.on('connect', function(){
  console.log("mqtt connected");
  carDB.find({}, function(err, resp){
    if(resp && resp.length){
      for(var i = 0; i < resp.length; i++){
        console.log("Subscribed to " + resp[i].car_id);
        client.subscribe(resp[i].car_id+constants.MQTT.RPM);
        client.subscribe(resp[i].car_id+constants.MQTT.SPEED);
        client.subscribe(resp[i].car_id+constants.MQTT.GRAVITATION_FORCE);
        client.subscribe(resp[i].car_id+constants.MQTT.LOCATION_COORDS);
      }
    }
  });
});

client.on('message', function(topic, payload) {
  // console.log(topic, payload);
  switch (parseTopic(topic)) {
    case constants.MQTT.RPM:
      var device_id = topic.substr(0, topic.length - 3);
      console.log(device_id);
      carDB.findOne({car_id: device_id}, function(err, car){
        if(car){
          var rpm = String(payload);
          console.log(rpm);
          if (rpm == 'none'){
            updateCar(device_id, {car_status: constants.CAR_STOPPED});
            
            //FCM
            if (!car.ignition_alert || car.ignition_alert == constants.FCM_FLAGS.IGNITION_RESET ||
              car.ignition_alert == constants.FCM_FLAGS.IGNITION_ON_SENT) {
              notify.usingUserId(car.subscriberid, 'Car stopped', 'Description', null, function(isSuccess) {
                updateCar(car._id, {ignition_alert: constants.FCM_FLAGS.IGNITION_OFF_SENT});
              })
            }
          } else if (typeof parseInt(rpm) == 'number'){
            // console.log(car);
            updateCar(device_id, {car_status: constants.CAR_RUNNING});
            
            if (car.current_trip) {
              tripDB.findById(car.current_trip, function(err, trip){
                // console.log(err, trip);
                if (trip && trip.status === constants.TRIP_STARTED) {
                  rpmDB.logRPM(trip._id, parseInt(rpm));
                }
              });
            }
            
            //FCM
            if (!car.ignition_alert || car.ignition_alert == constants.FCM_FLAGS.IGNITION_RESET ||
              car.ignition_alert == constants.FCM_FLAGS.IGNITION_OFF_SENT) {
              notify.usingUserId(car.subscriberid, 'Car started', 'Descriptiion', null, function(isSuccess) {
                updateCar(car._id, {ignition_alert: constants.FCM_FLAGS.IGNITION_ON_SENT});
              });
            }
          }
        }
      })
    break;
    case constants.MQTT.SPEED:
      var device_id = topic.substr(0, topic.length - 5);
      // console.log(device_id);
      carDB.findOne({car_id: device_id}, function(err, car){
        // console.log(car);
        if (car) {
          var speed = parseInt(String(payload)) == NaN ? String(payload) : parseInt(String(payload));
          if (car.speed_limit && (typeof speed == 'number' && speed > car.speed_limit)) {
            if (!car.speed_alert || car.speed_alert == constants.FCM_FLAGS.SPEED_LIMIT_NORMAL_SENT ||
              car.speed_alert == constants.FCM_FLAGS.SPEED_LIMIT_RESET) {
                // console.log("inloop")
              notify.usingUserId(car.subscriberid, 'Speed limit exceeded', 'Body', null, function(isSuccess){
                if (isSuccess) {
                  updateCar(car._id, {speed_alert: constants.FCM_FLAGS.SPEED_LIMIT_CROSSED_SENT});
                }
              });
            }
          } else if (car.speed_limit && (typeof speed == 'number' && speed < car.speed_limit)) {
            if(car.speed_alert == constants.FCM_FLAGS.SPEED_LIMIT_RESET || car.speed_alert == constants.FCM_FLAGS.SPEED_LIMIT_CROSSED_SENT) {
              notify.usingUserId(car.subscriberid, 'Speed limit normal', 'Description', null, function(isSuccess) {
                if (isSuccess) {
                  updateCar(car._id, {speed_alert: constants.FCM_FLAGS.SPEED_LIMIT_NORMAL_SENT});
                }
              });
            }
          }
        }
      });
    break;
    case constants.MQTT.GRAVITATION_FORCE:
    break;
    case constants.MQTT.LOCATION_COORDS:
      var device_id = topic.substr(0, topic.length - 4);
      carDB.findOne({car_id:device_id}, function(err, car) {
        // console.log(car);
        if (car && car.current_trip) {
          //12.33,23.33
          payload = String(payload);
          var split = payload.split(',');
          var lat = split[0];
          var long = split[1];
          var location = {
            lat: lat,
            long: long
          }
          tripDB.findById(car.current_trip, function(err, trip){
            if (trip && trip.status === constants.TRIP_STARTED) {
              tripDB.updatePath(trip._id, location);
            }
          });
          geofenceDB.find({subscriberid:car.subscriberid}, function(err, resp){
            if(resp && resp.length > 0) {
              for(var i=0; i<resp.length; i++) {
                var isIn = geolib.isPointInCircle(
                  {latitude: location.lat, longitude: location.long},
                  {latitude:resp[i].lat, longitude:resp[i].long},
                  resp[i].radius*1000
                );
                if (isIn && (!car.geofence_alert || car.geofence_alert == constants.FCM_FLAGS.GEOFENCE_OUT_SENT)) {
                  notify.usingUserId(car.subscriberid, 'Device '+ device_id +' is in geofence', 'Description',null, function(){
                    updateCar(car._id, {geofence_alert:constants.FCM_FLAGS.GEOFENCE_IN_SENT});
                  });
                } else if (!isIn &&(!car.geofence_alert || car.geofence_alert == constants.FCM_FLAGS.GEOFENCE_IN_SENT)) {
                  notify.usingUserId(car.subscriberid, 'Device '+ device_id +' is out of geofence', 'Description',null, function(){
                    updateCar(car._id, {geofence_alert:constants.FCM_FLAGS.GEOFENCE_OUT_SENT});
                  });
                }
              }
            }
          })
        }
      });
    break;
  }
});

module.exports = client;

function parseTopic (topic) {
  if (typeof topic != "string") {
    return '';
  } else if (topic.includes(constants.MQTT.RPM)){
    return constants.MQTT.RPM;
  } else if (topic.includes(constants.MQTT.GRAVITATION_FORCE)){
    return constants.MQTT.GRAVITATION_FORCE;
  } else if (topic.includes(constants.MQTT.LOCATION_COORDS)){
    return constants.MQTT.LOCATION_COORDS;
  } else if (topic.includes(constants.MQTT.SPEED)){
    return constants.MQTT.SPEED;
  } else {
    return '';
  }
}

function updateCar(id, update){
  carDB.findByIdAndUpdate(id, update, function(){});
}