var admin = require("firebase-admin");

var serviceAccount = require("../config/userapp-195008-firebase-adminsdk-hw02s-65f449ad5c.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://userapp-195008.firebaseio.com"
});

module.exports=admin;