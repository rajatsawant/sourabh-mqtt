const userDB = require('../models/user_data');
const parse = require('mongoose-error-helper').errorHelper;
const toolbox = require('../services/toolbox');
const constants = require('../services/constants');

module.exports = (router) => {
	//register
	router.post(constants.routes.user.REGISTER, (req, res) => {
		userDB.create(req.body, function (err, resp) {
			console.log(err);
			if (err) {
				toolbox.sendResponse(res, constants.ERROR, parse(err));
			} else {
				var user = JSON.parse(JSON.stringify(resp));
				toolbox.sendResponse(res, constants.SUCCESS, user);
			}
		});
	});

	//login
	router.post(constants.routes.user.LOGIN, (req, res) => {
		userDB.findOne({ emailid: req.body.emailid }, function (err, resp) {
			if (resp) {
				userDB.comparePassword(req.body.password, resp.password, function (err, isMatch) {
					if (isMatch) {
						if (req.body.fcm_token){
							userDB.update(resp._id, {fcm_token: req.body.fcm_token}, function(err, resp){
							});
						}
						toolbox.sendResponse(res, constants.SUCCESS, resp);
					} else if (err || !isMatch) {
						toolbox.sendResponse(res, constants.ERROR, parse(err), !isMatch && 'Wrong Password');
					}
				});
			} else {
				toolbox.sendResponse(res, constants.ERROR, err && parse(err), !err && 'User not found');
			}
		});
	});

	return router;
}