const geofenceDB = require('../models/geofence_data');
const parse = require('mongoose-error-helper').errorHelper;
const toolbox = require('../services/toolbox');
const constants = require('../services/constants');

module.exports = (router) => {
	//create geofence
	router.post(constants.routes.geofence.ADD, (req, res) => {
		geofenceDB.create(req.body, function(err, resp){
      if (err) {
        toolbox.sendResponse(res, constants.ERROR, parse(err), "Failed to create geofence");
      } else {
        toolbox.sendResponse(res, constants.SUCCESS, resp, "Geofence created");
      }
    });
	});

  //delete geofence
	router.post(constants.routes.geofence.DELETE, (req, res) => {
		geofenceDB.delete(req.body, function (err, resp) {
			if (err) {
				toolbox.sendResponse(res, constants.ERROR, parse(err), "Error deleting geofence");
			} else {
				toolbox.sendResponse(res, constants.SUCCESS, resp, "Geofence deleted");
			}
		});
	});

	//Get geofences by subscriber id
	router.post(constants.routes.geofence.GET_GEOFENCES_BY_SUBSCRIBER_ID, (req, res) => {
		geofenceDB.getGeofencesBySubscriberId(req.body.subscriberid, function (err, resp) {
			if (resp) {
				toolbox.sendResponse(res, constants.SUCCESS, resp);
			} else {
				toolbox.sendResponse(res, constants.ERROR, err ? parse(err) : null);
			}
		});
	});

	return router;
}