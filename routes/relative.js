const relativeDB = require('../models/relative_data');
const parse = require('mongoose-error-helper').errorHelper;
const toolbox = require('../services/toolbox');
const constants = require('../services/constants');

module.exports = (router) => {
	//create relative
	router.post(constants.routes.relative.ADD, (req, res) => {
		relativeDB.create(req.body, function (err, resp) {
			if (err) {
				console.log(err);
				toolbox.sendResponse(res, constants.ERROR, parse(err));
			} else {
				toolbox.sendResponse(res, constants.SUCCESS, resp);
			}
		});
	});

	//Delete relative
	router.post(constants.routes.relative.DELETE, (req, res) => {
		relativeDB.delete(req.body, function (err, resp) {
			if (resp) {
				toolbox.sendResponse(res, constants.SUCCESS, resp, "Relative deleted");
			} else {
				toolbox.sendResponse(res, constants.ERROR, err ? parse(err) : null, err ? null : "Relative not found");
			}
		});
	});

	//Get relatives by subscriber id
	router.get(constants.routes.relative.GET_RELATIVES_BY_SUBSCRIBER_ID, (req, res) => {
		relativeDB.getRelativesBySubscriberEmail(req.params.emailid, function (err, resp) {
			if (resp) {
				toolbox.sendResponse(res, constants.SUCCESS, resp);
			} else {
				toolbox.sendResponse(res, constants.ERROR, err ? parse(err) : null);
			}
		});
	});

	return router;
}