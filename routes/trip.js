const tripDB = require('../models/trip_data');
const carDB = require('../models/cars_data');
const parse = require('mongoose-error-helper').errorHelper;
const toolbox = require('../services/toolbox');
const constants = require('../services/constants');

module.exports = (router) => {
	//create trip
	router.post(constants.routes.trip.START_TRIP, (req, res) => {
		tripDB.startTrip(req.body, function (err, resp) {
			if (err) {
				toolbox.sendResponse(res, constants.ERROR, parse(err));
			} else {
				toolbox.sendResponse(res, constants.SUCCESS, resp);
				carDB.findByIdAndUpdate(req.body.deviceid, {current_trip: resp._id}, function(){});
			}
		});
	});

  //stop trip
	router.post(constants.routes.trip.STOP_TRIP, (req, res) => {
		tripDB.stopTrip(req.params, req.body, function (err, resp) {
			if (err) {
				toolbox.sendResponse(res, constants.ERROR, parse(err));
			} else {
				toolbox.sendResponse(res, constants.SUCCESS, resp);
			}
		});
	});

	//Delete trip
	router.post(constants.routes.trip.DELETE, (req, res) => {
		tripDB.delete(req.body, function (err, resp) {
			if (resp) {
				toolbox.sendResponse(res, constants.SUCCESS, resp, "Trip deleted");
			} else {
				toolbox.sendResponse(res, constants.ERROR, err ? parse(err) : null, err ? null : "Trip not found");
			}
		});
	});

	//Get cars by subscriber id
	router.get(constants.routes.trip.GET_TRIPS_BY_SUBSCRIBER_ID, (req, res) => {
		tripDB.getTripsBySubscriberId(req.params.id, function (err, resp) {
			if (resp) {
				toolbox.sendResponse(res, constants.SUCCESS, resp);
			} else {
				toolbox.sendResponse(res, constants.ERROR, err ? parse(err) : null);
			}
		});
	});

	return router;
}