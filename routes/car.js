const carsDB = require('../models/cars_data');
const parse = require('mongoose-error-helper').errorHelper;
const toolbox = require('../services/toolbox');
const constants = require('../services/constants');
const mqtt = require('../config/mqtt');

module.exports = (router) => {
	//create relative
	router.post(constants.routes.cars.ADD, (req, res) => {
		carsDB.create(req.body, function (err, resp) {
			console.log(err);
			if (err) {
				toolbox.sendResponse(res, constants.ERROR, parse(err));
			} else {
				mqtt.subscribe(resp.car_id);
				toolbox.sendResponse(res, constants.SUCCESS, resp);
			}
		});
	});

	//Delete car
	router.post(constants.routes.cars.DELETE, (req, res) => {
		carsDB.delete(req.body, function (err, resp) {
			if (resp) {
				toolbox.sendResponse(res, constants.SUCCESS, resp, "Relative deleted");
			} else {
				toolbox.sendResponse(res, constants.ERROR, err ? parse(err) : null, err ? null : "Relative not found");
			}
		});
	});

	//Get cars by subscriber id
	router.post(constants.routes.cars.GET_CARS_BY_SUBSCRIBER_ID, (req, res) => {
		carsDB.getCarsbySubscriberId(req.body.subscriberid, function (err, resp) {
			if (resp) {
				toolbox.sendResponse(res, constants.SUCCESS, resp);
			} else {
				toolbox.sendResponse(res, constants.ERROR, err ? parse(err) : null);
			}
		});
	});

	router.post(constants.routes.cars.SET_SPEED_LIMIT, (req, res) => {
		carsDB.setSpeedLimit(req.body.car_id, req.body.speed_limit, function(err, resp){
			if(resp)
				toolbox.sendResponse(res, constants.SUCCESS, resp, 'Speed limit set');
			else 
				toolbox.sendResponse(res, constants.ERROR, err ? parse(err) : null, 'Error setting speed');
		});
	});

	return router;
}