const express = require('express'); 
const app = express();  
var cookieParser = require('cookie-parser');
var exphbs = require('express-handlebars');
var flash = require('connect-flash');
var session = require('express-session');
var passport = require('passport');

var mongoose = require('mongoose');
var expressValidator = require('express-validator');

const constants = require('./services/constants');

// Connection to DB
mongoose.connect('mongodb://localhost:27017/leo');

mongoose.connection.on('connected', () => {
  console.log("connected to db");
  mqtt = require('./config/mqtt');
});

mongoose.connection.on('error', (err) => {
  if (err) {
    console.log("failed to connect db"+err);
  }
});
//var url = "mongodb://localhost:27017/";
var cors = require('cors');
const morgan = require('morgan')
const bodyParser = require('body-parser')
var path = require('path');
var port = 2000; 

var userRoute = require('./routes/user')(express.Router());
var relativeRoute = require('./routes/relative')(express.Router());
var carRoute = require('./routes/car')(express.Router());
var tripRoute = require('./routes/trip')(express.Router());
var geofenceRoute = require('./routes/geofence')(express.Router());

// view engine
app.set('views', path.join(__dirname, 'views'));
app.engine('handlebars', exphbs({defaultLayout:'layout'}));
app.set('view engine', 'handlebars');
// body parser middelware
app.use(bodyParser.json())
app.use(morgan('dev'))
app.use(cookieParser());

app.use(express.static(path.join(__dirname,'public') ));  
app.use(cors());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use (session({
secret: 'secret',
saveUninitialized: true,
resave: true
})
);

app.use(passport.initialize());
app.use(passport.session());
// middelewares
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

app.use(flash());
app.use(constants.URL + '/subscriber', userRoute);
app.use(constants.URL + '/relative', relativeRoute);
app.use(constants.URL + '/mycar', carRoute);
app.use(constants.URL + '/subscribertrip', tripRoute);
app.use(constants.URL + '/geofence', geofenceRoute);


app.use(function(req,res,next){
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  next();
});

app.listen(port,()=>{console.log("app on"+port); });